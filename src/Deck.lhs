\ignore{
\begin{code}
{-# LANGUAGE TupleSections #-}

module Deck
  ( SomeCards
  , IndexedCard
  , shuffle
  , deal
  , infiniteDeck
  , standartDeck
  )
where

import           System.Random.Shuffle          ( shuffleM )
import           Utils
\end{code}
}

The following data structures are used in the basic representation of a collection of playing cards. \ttt{IndexedCard} allows us to track the deck from which a card originates in case the problem instance requires dealing more than 52 cards.\\

\begin{code}
data Suit = Diamonds | Clubs | Hearts | Spades deriving (Eq, Ord, Enum, Show, Bounded)

data Rank = Two | Three | Four | Five | Six | Seven | Eight | Nine | Ten
               | Jack | Queen | King | Ace deriving (Eq, Ord, Enum, Show, Bounded)

data Card = Card Suit Rank deriving (Eq, Ord)

type IndexedCard = (Int, Card)
type SomeCards = [IndexedCard]
\end{code}

Some rudimentary operations (\ttt{shuffle}, \ttt{deal}) as well as an infinite stream of playing cards are provided.\\

\begin{showCode}
allCards :: [Card]
allCards = ...

infiniteDeck :: SomeCards
infiniteDeck = [ (i, c) | i <- [1 ..], c <- allCards ]

shuffle :: SomeCards -> IO SomeCards
shuffle = ...

deal :: [Int] -> SomeCards -> [SomeCards]
deal = ...
\end{showCode}

\ignore{
\begin{code}
allCards :: [Card]
allCards =
  [ Card s r
  | s <- [(minBound :: Suit) .. (maxBound :: Suit)]
  , r <- [(minBound :: Rank) .. (maxBound :: Rank)]
  ]

standartDeck :: SomeCards
standartDeck = map (1, ) allCards

infiniteDeck :: SomeCards
infiniteDeck = [ (i, c) | i <- [1 ..], c <- allCards ]

shuffle :: SomeCards -> IO SomeCards
shuffle = shuffleM

deal :: [Int] -> SomeCards -> [SomeCards]
deal = splitPlaces
\end{code}
}

Although generally a bad practice, pretty printing for cards is implemented directly as a \ttt{Show} instance. Besides being a quick and dirty solution, this also comes off as a workaround so that Unicode characters (e.g. $\lozenge$, $\heartsuit$) could be displayed without the need to introduce additional dependencies to libraries like \ttt{Text}.\\

\begin{showCode}
prettyShowSuit :: Suit -> String
prettyShowSuit Diamonds = "%$\blacklozenge$%"
prettyShowSuit Clubs	= ...

prettyShowRank :: Rank -> String
prettyShowRank Two   = "2"
prettyShowRank Three = ...

prettyShowCard :: Card -> String
prettyShowCard (Card s r) = prettyShowSuit s ++ prettyShowRank r

instance Show Card where
	show = prettyShowCard

\end{showCode}

\ignore{
\begin{code}
prettyShowSuit :: Suit -> String
prettyShowSuit Diamonds = "♦"
prettyShowSuit Clubs    = "♣"
prettyShowSuit Hearts   = "♥"
prettyShowSuit Spades   = "♠"

prettyShowRank :: Rank -> String
prettyShowRank Two   = "2"
prettyShowRank Three = "3"
prettyShowRank Four  = "4"
prettyShowRank Five  = "5"
prettyShowRank Six   = "6"
prettyShowRank Seven = "7"
prettyShowRank Eight = "8"
prettyShowRank Nine  = "9"
prettyShowRank Ten   = "10"
prettyShowRank Jack  = "J"
prettyShowRank Queen = "Q"
prettyShowRank King  = "K"
prettyShowRank Ace   = "A"

prettyShowCard :: Card -> String
prettyShowCard (Card s r) = prettyShowSuit s ++ prettyShowRank r

instance Show Card where
  show = prettyShowCard

\end{code}
}
