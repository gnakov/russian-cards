\subsection{Typed wrapper for HaskellForMaths Fields}

\ignore{
\begin{code}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeApplications #-}

\end{code}
}
The purpose of this module is to emulate the type $\prod(q:\ttt{prime power})GF(q)$.

First we will import the internal GHC module \ttt{GHC.TypeLits} for working with type-level natural numbers and \ttt{Data.Proxy} for proxies respectively.\\
\begin{code}
module TypedMath where

import           GHC.TypeLits
import           Data.Proxy
\end{code}

\ignore{
\begin{code}
import           Data.Kind
import           Data.List                      ( find )
import           Data.Maybe                     ( isJust )
import           Math.Core.Field
import           Math.Core.Utils                ( FinSet, elts )
import           Language.Haskell.TH
\end{code}
}

\ttt{M} is type function mapping some type promoted prime power $3 \leq q' \leq 5^2$ (of kind \ttt{Nat}) \footnote{$x^T$ denotes the type of $x$; $y'$ implicitly states that $y$ is type-promoted natural number} to the appropriate finite field type $GF(q)^T$ in the \ttt{HaskellForMaths} package : $q' \mapsto GF(q)^T$.\\

\begin{showCode}
type family M (n::Nat)::Type where
    M 2 = F2
    ...
    M 23 = F23
    M 25 = F25
\end{showCode}

Note that \ttt{M} is a partial function, but the method \ttt{getTypedField::Proxy a $\to$ [M a]} of the type class \ttt{TypedField} will force the compiler to check that \ttt{M} is defined for every \ttt{x'} for which there exists an instance \ttt{instance TypedField x'}.\\

\ignore{
\begin{code}
type family M (n::Nat)::Data.Kind.Type where
    M 2 = F2
    M 3 = F3
    M 4 = F4
    M 5 = F5
    M 7 = F7
    M 8 = F8
    M 9 = F9
    M 11 = F11
    M 13 = F13
    M 16 = F16
    M 17 = F17
    M 19 = F19
    M 23 = F23
    M 25 = F25
\end{code}
}

\begin{code}
class ( Show (M a),
        Eq (M a),
        Fractional (M a),
        Num (M a),
        Ord (M a),
        FinSet (M a) )
        => TypedField a
        where

    getTypedField :: Proxy a -> [M a]
    getTypedField _ = elts

\end{code}

\ttt{TypedField} bundles together all the type classes elements of a finite field $GF(q)$ are instances of and provides access to the said elements. The \ttt{HaskellForMaths} class \ttt{FinSet} is the required constraint for any kind of computation involving finite fields. The instances of \ttt{TypedField} are declared directly on the prime power $q'$ and not the $GF(q')^T$.\\

\ignore{
\begin{code}

instance TypedField 3
instance TypedField 4
instance TypedField 5
instance TypedField 7
instance TypedField 8
instance TypedField 9
instance TypedField 11
instance TypedField 13
instance TypedField 16
instance TypedField 17
instance TypedField 19
instance TypedField 23
instance TypedField 25
\end{code}
}

\begin{showCode}
instance TypedField 3

instance TypedField 4

instance TypedField 5
...
\end{showCode}

\ttt{SomeField} is an existential "box" pairing a type-level prime power $p'$ (stored as a \ttt{Proxy p'}) with the instance \ttt{TypedField p'}.\\
\begin{code}
data SomeField
   = forall (n::Nat) . (KnownNat n, TypedField n) => SomeField (Proxy n)
\end{code}

\ttt{validFields} contains a \ttt{SomeField} object for each prime power $q$ for which\\ \ttt{HaskellForMaths} provides an implementation of $GF(q)$.\\

\begin{showCode}
validFields :: [SomeField]
validFields =
  [ SomeField (Proxy @3)
  , SomeField (Proxy @4)
  ,  ...
  , SomeField (Proxy @25)
  ]
\end{showCode}

\ignore {
\begin{code}

validPrimePowers :: [Int]
validPrimePowers = [3, 4, 5, 7, 8, 9, 11, 13, 16, 17, 19, 23, 25]

validFields :: [SomeField]
validFields =
  [ SomeField (Proxy @3)
  , SomeField (Proxy @4)
  , SomeField (Proxy @5)
  , SomeField (Proxy @7)
  , SomeField (Proxy @8)
  , SomeField (Proxy @9)
  , SomeField (Proxy @11)
  , SomeField (Proxy @13)
  , SomeField (Proxy @16)
  , SomeField (Proxy @17)
  , SomeField (Proxy @19)
  , SomeField (Proxy @23)
  , SomeField (Proxy @25)
  ]
\end{code}
}

\ttt{selectField} emulates a value of the product type $\prod(q:\ttt{prime power})GF(q)$. \footnote {this is not strictly true. For simplicity we ignore the fact that we are returning a list of $GF(q)$ elements wrapped into an optional value of an existential type.}\\

\begin{code}
selectField :: Int -> Maybe SomeField
selectField n =
  someNatVal (toInteger n)
    >>= (\(SomeNat p1) -> find (\(SomeField p2) -> same p1 p2) validFields)
 where
  same :: (KnownNat a, KnownNat b) => Proxy a -> Proxy b -> Bool
  same a b = isJust $ sameNat a b
\end{code}

Given an arbitrary integer $q$, we first promote it to a type-level natural $q'$ and then match it over the list \ttt{validFields} using \ttt{sameNat} from \ttt{GHC.TypeLits}:\\
\begin{showCode}
sameNat :: (KnownNat a, KnownNat b) => Proxy a -> Proxy b -> Maybe (a :~: b)
\end{showCode}

We just check against the \ttt{Maybe} constructor, as we are not interested into the witness of the type equality. Returning a value of the existential \ttt{SomeField} instead of the direct \ttt{Proxy k} is crucial. In the latter case, the function's signature would be\\ \ttt{selectField :: forall (k::Nat). Int $\to$ Proxy k} which denotes that the caller dictates the choice of the type \ttt{k}. This is the opposite of what we want to achieve, namely given an integer, \ttt{selectField} itself determines the appropriate \ttt{k} type.

Finally, some unwrapping and boilerplate code is required to actually operate on the $GF(q)$ elements. Typical usage pattern is:\\

\begin{showCode}
let x = selectField 4
case x of {(Just (SomeField f)) -> show $ getTypedField f}
--"[0,1,a4,a4+1]"
\end{showCode}
