\ignore{
\begin{code}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE PolyKinds #-}

module Utils where

import           Data.Bifunctor
\end{code}
}

\ttt{IntParam} is a typesafe integer wrapper. \ttt{Num} instance is automatically derived, so that arithmetic operations may be performed directly without unwrapping, although under certain circumstances this could lead to bypassing the typechecking constraints.\\

\begin{code}
newtype IntParam a = IntP {getInt :: Int} deriving (Eq, Ord, Show, Num, Read)
\end{code}

$\text{powerOf n b} =
  \begin{cases}
    \log_b n & \text{if there exists } x \in \mathbb{N}, \text{ s.t. } n=b^x  \\
    \bot & \text{otherwise}
  \end{cases}$\\

\begin{code}
powerOf :: Int -> Int -> Maybe Int
powerOf n b =
  let (k, p) = until ((>= n) . fst) (bimap (* b) (+ 1)) (1, 0)
  in  if k == n then Just p else Nothing
\end{code}

$\text{listPowerOf n $(b_1,\dots, b_k)$} =
\begin{cases}
(b_i, \log_{b_i} n)  & \text{if there exists } i,y \in \mathbb{N}, i \leq k \text{ s.t. } n = b_i^y  \\
\bot & \text{otherwise}
\end{cases}$\\

\begin{code}
listPowerOf :: Int -> [Int] -> Maybe (Int, Int)
listPowerOf _ []       = Nothing
listPowerOf n (b : bs) = case n `powerOf` b of
  Just p  -> Just (b, p)
  Nothing -> listPowerOf n bs
\end{code}

\begin{code}
maybeToEither :: a -> Maybe b -> Either a b
maybeToEither a Nothing  = Left a
maybeToEither _ (Just b) = Right b

splitPlaces :: [Int] -> [e] -> [[e]]
splitPlaces []       _  = []
splitPlaces _        [] = []
splitPlaces (x : xs) l  = take x l : splitPlaces xs (drop x l)
\end{code}
