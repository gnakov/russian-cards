
\subsection{Preliminaries in Haskell}
We will focus on the existing tools in Haskell that facilitate type-level programming.

\subsubsection{Phantom Types and Proxies}

A phantom type is a parametrized type for which at least one parameter does not appear on the right-hand side of its definition. Consider:\\

\begin{showCode}
  data Expression a = IntConst Int | BoolConst Bool
\end{showCode}

One possible use of phantom types is to encode various well-formedness checks in the type system.\\
\begin{showCode}
  plus :: Expression Int -> Expression Int -> Expression Int

  let x = IntConst 5 :: Expression Int
      y = BoolConst True :: Expression Bool
  in plus x y 
  -- type error
     plus x x
  -- ok
\end{showCode}

Defined this way \ttt{plus} operates only on integer expressions.\footnote{Note we can get rid off the explicit type annotations for \ttt{x,y} by using Generalized Algebraic Datatypes to further reify the type constructors \ttt{IntConst} and \ttt{BoolConst} to \ttt{Int$\to$Expression Int} and \ttt{Bool$\to$Expression Bool} respectively.}

Proxies are phantom types with the sole purpose of storing only the type information.\\

\begin{showCode}
  data Proxy a = Proxy
\end{showCode}


\subsubsection{Datatype promotion and kind polymorphism}
GHC allows automatic promotion of every suitable datatype to a kind, and its (value) constructors to type constructors \cite{promotion}. To put it simply, when we say: \\

\begin{showCode}
  data Nats = Zero | Succ Nats
\end{showCode}

we declare two entities: a type \ttt{Nats} inhibited by \ttt{Zero} and \ttt{Succ::Nats $\to$ Nats}; and a kind \ttt{Nats} inhibited by two empty types \ttt{'Zero} and \ttt{'Succ::Nats $\to$ Nats}. Note that only types of kind \ttt{*} are inhibitable.
Datatype promotion is the main tool we use to transition runtime values to types to leverage rich type-level programming.
\begin{figure}[H]
  \centering
  \includegraphics[scale=0.55]{promotion.png}
  \caption{Datatype promotion of \ttt{Nats}}.
\end{figure}

Kind polymorphism (enabled with \ttt{-XPolyKinds}) introduces kind variables and lets the compiler infer the most general polymorphic kind on undecorated declarations. Thus, we could use the promoted datatypes in places where GHC would typically expect a type of kind \ttt{*}. Compare:\\

\begin{showCode}
data UnikindedProxy a = P
:k UnikindedProxy 
-- UnikindedProxy :: * -> *
:t P
-- P :: UnikindedProxy a
\end{showCode}
with \\
\begin{showCode}
  :set -XPolyKinds
  data UnikindedProxy a = P
  :k UnikindedProxy 
  --UnikindedProxy :: k -> *
  :t P
  --P :: forall k (a :: k). UnikindedProxy a
\end{showCode}

\subsubsection{Existential Types}

Existential types provide a way to use type variables that appear only on the right hand side of datatype/newtype declaration. Normally, every type variable on the rhs must also appear on the left hand side, too. Consider:\\
\begin{showCode}
  data Container a b = Box a b
\end{showCode}
We can turn off this behaviour by existential quantification:\\
\begin{showCode}
  data Container a = forall b. Box a b
\end{showCode}

Effectively it means we can hide an opaque type (in our case \ttt{b}) within another type (\ttt{Container a}). The consumer of the data does not have any knowledge on which type was used during the construction of the existential.

Semantically, the declaration above can be read as $\forall a \exists b \, \ttt{Box a b}$ - for any type \ttt{a} there exists a type \ttt{b} with which we construct a new value \ttt{Box a b} of type \ttt{Container a}. Note there is no guarantee which exactly type \ttt{b} is - the producer is free to choose it.

