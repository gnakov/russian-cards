\subsection{Main and Example Output}
\ignore{
\begin{code}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE ExplicitForAll #-}
{-# LANGUAGE GADTs #-}
\end{code}
}

We will need again proxies and type-level naturals.\\

\begin{code}
module Main where

import           Data.Proxy
import           GHC.TypeLits
\end{code}

\ignore{
\begin{code}
import           Data.List
import           Data.Bifunctor                 ( bimap )
import           Data.Maybe                     ( fromJust )
import           Data.Tuple                     ( swap )
import           Data.Function                  ( on )
import           System.Random                  ( randomRIO )
import           System.Random.Shuffle          ( shuffleM )
import           Math.Core.Utils                ( FinSet )
import           Math.Combinatorics.FiniteGeometry

import           Deck
import           Utils
import           TypedMath
\end{code}
}

\begin{code}
data Agents = A | B | C deriving (Eq, Show)
data Params = PQ | Alpha | Delta deriving (Eq, Show)

type DealSizeA = IntParam 'A
type DealSizeB = IntParam 'B
type DealSizeC = IntParam 'C

type ParamQ = IntParam 'PQ
type ParamAlpha = IntParam 'Alpha
type ParamDelta = IntParam 'Delta
\end{code}

The Russian Cards problem instances are typically described by quite a few integral parameters whose usage could quite easily become untraceable. That's why we wrap each of them into \ttt{newtype}-s and rename it properly. While simple \ttt{type} redefinitions of \ttt{Int} (e.g. \ttt{type ParamQ = Int}) would also suffice to improve readability, they would not actually force the compiler to type check the purposeful semantics - we could still easily pass 5 to a function expecting \ttt{ParamQ} and do nonsensible arithmetic in it. The use of the phantom type in the newtype \ttt{IntParam} prevents exactly this issue at the cost of added wrapping/unwrapping boilerplate.\\

\begin{code}
data ProblemInstance :: * -> * where
  ProblemInstance :: (Ord a, Num a, FinSet a, Show a) => {
    q'     :: ParamQ,
    alpha' :: ParamAlpha,
    delta' :: ParamDelta,
    field' :: [a]
  } -> ProblemInstance a

-- deriving instance Show (ProblemInstance a)
\end{code}

\ttt{ProblemInstance a} is a generalized abstract datatype storing the main parameters of the problem. Generally speaking, GADTs allows us to reify the types of different constructors of a given ADT. It is especially useful when paired with phantom types. Consider the following:\\

\begin{showCode}
data Expression a where
  IntConst 	:: Int -> Expression Int,
  BoolConst :: Bool -> Expression Bool,
  Add :: Expression Int -> Expression Int -> Expression Int,
  TestEq :: Expression Int -> Expression Int -> Expression Bool
\end{showCode}

Without GADTs all we could have written would be \ttt{Add :: Expression a -> Expression a -> Expression a} which does not convey nearly enough information to the type system and we would be forced to handle the faulty cases by manual pattern matching. \\
However, this is not the aspect we use in \ttt{ProblemInstance a}. GADTs are employed so we can express constraints in the declaration of the datatype.\\
We prefer the approach of GADTs with an explicit type parameter \ttt{a} for $GF(q)^T$ over an existential type wrapping \ttt{a} for reasons that would become apparent later.\\

\ignore{
\begin{code}
totalDealSize :: (DealSizeA, DealSizeB, DealSizeC) -> Int
totalDealSize (IntP a, IntP b, IntP c) = a + b + c
\end{code}
}

\begin{code}
informative :: ParamQ -> ParamAlpha -> DealSizeC -> Bool
informative (IntP q) (IntP alpha) (IntP c) = c < q ^ alpha - q ^ (alpha - 1)

secure :: ParamQ -> ParamAlpha -> ParamDelta -> DealSizeC -> Bool
secure (IntP q) (IntP alpha) (IntP delta) (IntP c) = c < q ^ (delta - alpha)
\end{code}
\ttt{informative} and \ttt{secure} are straightforward implementation of the informativity and security conditions.\\

\ttt{makeProblemInstance} is a smart constructor for a \ttt{ProblemInstance}.\\
\begin{code}
makeProblemInstance
  :: forall (k :: Nat)
   . (KnownNat k, TypedField k)
  => (DealSizeA, DealSizeB, DealSizeC)
  -> Proxy k
  -> Either String (ProblemInstance (M k))
makeProblemInstance s@(a, b, c) f
  | getInt a <= 1 || getInt b <= 0 || getInt c <= 0 = Left
    "Use only positive hand sizes! Alice's hand size must be bigger than 2!"
  | otherwise = do
    let a' = getInt a
    (q, alpha) <- mkQA a'
    delta      <- mkDelta (totalDealSize s) $ getInt q
    if
      | not $ secure q alpha delta c -> Left
        "Problem instance not secure!"
      | not $ informative q alpha c -> Left
        "Problem instance not informative!"
      | otherwise -> return $ ProblemInstance q alpha delta $ getTypedField f
 where
  mkQA x =
    maybeToEither "Are you sure Alice's hand size is a prime power?"
      $   bimap IntP IntP
      <$> listPowerOf x validPrimePowers
  mkDelta t q = maybeToEither
    ( "Are you sure that the total cards dealt and Alice's hand"
      ++ " size are powers of the same prime?" )
    (IntP <$> t `powerOf` q)
\end{code}
We check whether a solvable instance is obtainable from a given deal by ensuring the instance:
\begin{itemize}
  \item is valid: $a = q^{\alpha}$ and $a + b + c = q^{\delta}$ for $\alpha,\delta \in N$, $3\leq q \leq 5^2$ and $q$ - prime power
  \item is informative and secure
\end{itemize}

Before calling \ttt{makeProblemInstance} one should first construct the \ttt{f} argument of type \ttt{Proxy k}, where \ttt{k} is the promoted prime power $q'$ from the user input. \ttt{makeProblemInstance} uses $\ttt{f}$ to get the $GF(q)$ elements by calling \ttt{getTypedField}.

The next set of functions takes care of encrypting Alice's hand.\\
\begin{code}
choosePlane :: (Ord f, Num f, FinSet f) => ProblemInstance f -> IO [[f]]
choosePlane p = do
  let a          = getInt . alpha' $ p
      d          = getInt . delta' $ p
      f          = field' p
      alphaPlane = flatsAG d f a
  r <- randomRIO (0, length alphaPlane - 1)
  return (closureAG (alphaPlane !! r))

assignRandomPts :: SomeCards -> [[f]] -> IO [(IndexedCard, [f])]
assignRandomPts l xs = shuffleM l >>= (\x -> return $ zip x xs)

encryptA
  :: (Ord f, Num f, FinSet f)
  => ProblemInstance f
  -> [SomeCards]
  -> IO [(IndexedCard, [f])]
encryptA _ []             = return []
encryptA p (aDeal : restDeals) = do
  let d = getInt . delta' $ p
      f = field' p
  aFlat      <- choosePlane p
  aPoints    <- assignRandomPts aDeal aFlat
  restPoints <- assignRandomPts (concat restDeals) $ ptsAG d f \\ aFlat
  return (sortBy (compare `on` snd) (aPoints ++ restPoints))

announceA
  :: (Ord f, Num f, FinSet f)
  => ProblemInstance f
  -> [SomeCards]
  -> IO [SomeCards]
announceA p xs = do
  let d          = getInt . delta' $ p
      a          = getInt . alpha' $ p
      f          = field' p
      alphaPlane = flatsAG d f a
  pairs <- encryptA p xs
  return (map (findLabel pairs . closureAG) alphaPlane)
 where
  findLabel vks ks =
    let kvs = map swap vks in fromJust <$> map (`lookup` kvs) ks
\end{code}

\ttt{choosePlane} computes all affine $\alpha$-dimensional subspaces of the vector space $GF(q)^{\delta}$ by calling \ttt{flatsAG} and returns an arbitrary one. \ttt{encryptA} takes the three dealt hands, calls \ttt{choosePlane} to pick an $\alpha$-subspace and assigns Alice's cards to random points in it. Then it computes all the points in $GF(q)^{\delta}$, removes the ones in the subspace and pairs the remaining ones randomly with Bob's and Cath's cards. As a result we obtain list of (card, point) pairs. It is \ttt{announceA}'s responsibility to remap the points back to cards and build human readable announcement for Alice.

Note that at this point, the benefits of modeling the problem as a GADT with type parameters (namely \ttt{ProblemInstance f}) become obvious.

Suppose \ttt{ProblemInstance f} was an existential type (i.e. \ttt{ProblemInstance = forall f. ProblemInstance $\dots$ [f]} where \ttt{f} is an opaque type for $GF(q)^T$), we would be forced to create another existential datatypes for the output of \ttt{choosePlane} and \ttt{announceA} since \ttt{f} is no longer available in scope (\ttt{(IndexedCard, [f])} wouldn't be a valid construction). In particular, we would end up with something similar to:\\

\begin{showCode}
  data CardPoint = forall f. CardPoint IndexedCard [f]
\end{showCode}

However, under these circumstances \ttt{encryptA} could not sort the card points in the output list, as each element might encapsulate different type. There is no way to remedy the situation as the wrapped types are completely opaque to type system and there is no encoded guarantee that they are the same anymore.

The implemented GADT approach with explicit type parameter suffers no such drawbacks.

We continue with the function for decrypting an announcement.\\
\begin{code}
decrypt :: SomeCards -> [SomeCards] -> Either [SomeCards] SomeCards
decrypt ownHand xs =
  let d = filter (null . intersect ownHand) xs
  in  if length d == 1 then Right (head d) else Left d
\end{code}

\ttt{decrypt} takes a hand of cards  \ttt{ownHand} and a list of potential hands \ttt{xs} and retains only those hands in \ttt{xs} which have empty intersection with \ttt{ownHand}.\\

\begin{code}
data KnowledgeC = KnowledgeC {
    aCards' :: SomeCards,
    bCards' :: SomeCards
} deriving (Eq, Show)

cKnowledge :: SomeCards -> SomeCards -> [SomeCards] -> KnowledgeC
cKnowledge cHand deck announcement =
  let rest   = deck \\ cHand
      aCards = foldl1' intersect announcement
      bCards = rest \\ foldl1' union announcement
  in  KnowledgeC aCards bCards

isZeroKnowledge :: KnowledgeC -> Bool
isZeroKnowledge (KnowledgeC [] []) = True
isZeroKnowledge _                  = False
\end{code}

Given Cath's hand \ttt{cHand}, the full deck and the Alice's potential hands, \ttt{cKnowledge} models Cath's knowledge after the public announcement. The \ttt{KnowledgeC} data type contains the cards which Cath could learn with certainty that Alice and Bob are holding respectively.\\

Finally, \ttt{main} chains together all the user interaction and error handling.\\

\begin{code}
main :: IO ()
main = do
  a <- input "Alice" :: IO DealSizeA
  b <- input "Bob" :: IO DealSizeB
  c <- input "Cath" :: IO DealSizeC
  let dealSizes = (a, b, c)
      proxyA    =  listPowerOf (getInt a) validPrimePowers >>= selectField . fst

  case proxyA of
    Nothing -> printErr
      "Are you sure Alice's hand size is power of a prime between 3 and 25?"
    (Just (SomeField f)) -> case makeProblemInstance dealSizes f of
      Left  msg -> printErr msg
      Right p   -> do
        deck <- shuffle $ take (totalDealSize dealSizes) infiniteDeck
        let deals@[aHand, bHand, cHand] =
              deal [getInt a, getInt b, getInt c] deck
\end{code}

First the user inputs Alice's, Bob's and Cath's hand sizes \ttt{a,b,c} respectively and we check that $a=q^\alpha$ for some $q$ prime power, $\alpha \in \mathbb{N}$ and attempt to obtain a solvable Russian Cards Problem instance. If successful, we proceed with dealing hands from a deck containing $|a + b + c|$ many cards.\\

\begin{code}
        printRuler
        printHand "Alice" aHand
        printHand "Bob"   bHand
        printHand "Cath"  cHand
        printRuler

        --putStrLn "Alice announces that her hand is one of the following: "
        announcement <- announceA p deals
        putStrLn $ "Alice announces that her hand is one of the following "
                   ++ show  (length announcement ) ++ ": "
        mapM_ print announcement
        printRuler

        let bAlternatives = decrypt bHand announcement
            cAlternatives = decrypt cHand announcement

        either
          (\alternatives -> do
            putStrLn
              "Bob cannot decrypt. He cannot distinguish between the following:"
            mapM_ print alternatives
          )
          (\decrypted -> do
            let cHandByB = (deck \\ bHand) \\ decrypted
            printPrefix "Bob successfully decrypts Alice's hand:" decrypted
            printPrefix "Bob announces Cath's hand:"              cHandByB
          )
          bAlternatives

        printRuler

        either
          (\alternatives -> do
            let k@(KnowledgeC aCards bCards) =
                  cKnowledge cHand deck alternatives
            if isZeroKnowledge k
              then putStrLn "Cath does not know anything."
              else do
                printPrefix "Cath for sure knows that Alice holds:" aCards
                printPrefix "Cath for sure knows that Bob holds:"   bCards
          )
          (\decrypted -> do
            let bHandByC = (deck \\ cHand) \\ decrypted
            printPrefix "Cath succesfully decrypts Alice's hand:" decrypted
            printPrefix "Cath succesfully decrypts Bob's hand:"   bHandByC
          )
          cAlternatives

        printRuler
\end{code}

The above code handles pretty printing Alice's announcement and Bob's and Cath's subsequent decryption.

\ignore {
\begin{code}
 where
  input :: String -> IO (IntParam a)
  input s = IntP <$> do
    putStrLn $ "Please enter how many cards are dealt to " ++ s ++ ":"
    x <- getLine
    return (read x :: Int)

  printRuler = putStrLn $ replicate 40 '-'

  printPrefix msg x = putStrLn $ msg ++ " " ++ show x

  printHand name    = printPrefix (name ++ " gets:\r\n")

  printErr msg =
    putStrLn
      $  "Error:\r\n"
      ++ msg
      ++ "\r\nCannot solve this instance. Aborting ..."
\end{code}
}

An example run on the smallest possible\footnote{with regards to the deck size} problem instance our method can solve produces:\\
\begin{showCode}
Please enter how many cards are dealt to Alice:
3
Please enter how many cards are dealt to Bob:
5
Please enter how many cards are dealt to Cath:
1
----------------------------------------
Alice gets:
[(1,%$\blacklozenge$%10),(1,%$\blacklozenge$%6),(1,%$\blacklozenge$%5)]
Bob gets:
[(1,%$\blacklozenge$%7),(1,%$\blacklozenge$%2),(1,%$\blacklozenge$%8),(1,%$\blacklozenge$%9),(1,%$\blacklozenge$%4)]
Cath gets:
[(1,%$\blacklozenge$%3)]
----------------------------------------
Alice announces that her hand is one of the following 12:
[(1,%$\blacklozenge$%6),(1,%$\blacklozenge$%4),(1,%$\blacklozenge$%9)]
[(1,%$\blacklozenge$%6),(1,%$\blacklozenge$%7),(1,%$\blacklozenge$%3)]
[(1,%$\blacklozenge$%6),(1,%$\blacklozenge$%5),(1,%$\blacklozenge$%10)]
[(1,%$\blacklozenge$%2),(1,%$\blacklozenge$%7),(1,%$\blacklozenge$%10)]
[(1,%$\blacklozenge$%2),(1,%$\blacklozenge$%5),(1,%$\blacklozenge$%9)]
[(1,%$\blacklozenge$%2),(1,%$\blacklozenge$%4),(1,%$\blacklozenge$%3)]
[(1,%$\blacklozenge$%8),(1,%$\blacklozenge$%5),(1,%$\blacklozenge$%3)]
[(1,%$\blacklozenge$%8),(1,%$\blacklozenge$%4),(1,%$\blacklozenge$%10)]
[(1,%$\blacklozenge$%8),(1,%$\blacklozenge$%7),(1,%$\blacklozenge$%9)]
[(1,%$\blacklozenge$%6),(1,%$\blacklozenge$%2),(1,%$\blacklozenge$%8)]
[(1,%$\blacklozenge$%4),(1,%$\blacklozenge$%7),(1,%$\blacklozenge$%5)]
[(1,%$\blacklozenge$%9),(1,%$\blacklozenge$%10),(1,%$\blacklozenge$%3)]
----------------------------------------
Bob successfully decrypts Alice's hand: [(1,%$\blacklozenge$%6),(1,%$\blacklozenge$%5),(1,%$\blacklozenge$%10)]
Bob announces Cath's hand: [(1,%$\blacklozenge$%3)]
----------------------------------------
Cath does not know anything.
----------------------------------------
\end{showCode}
